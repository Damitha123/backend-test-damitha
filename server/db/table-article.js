var mysql = require('mysql');

var con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'backendTest'
});

con.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");

    var sql = "CREATE TABLE article (id INT AUTO_INCREMENT PRIMARY KEY, author_id INT(11), title VARCHAR(255), url VARCHAR(255), content VARCHAR(255), createdAt DATE, updatedAt DATE)";
    con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Table created");
        console.log(result);
    });
});