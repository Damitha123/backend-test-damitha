var express = require('express');
var bodyParser = require('body-parser');

var connect = require('./../server/db/connection');

var app = express();

app.use(bodyParser.json());

// create
app.post('/create/author', function (req, res) {
    var body = {};
    body.name = req.body.name;

    var sql = "INSERT INTO author (name) VALUES ('" + body.name + "')";
    connect.query(sql, function (err, result) {
        if (!body.name) {
            res.status(400).send({
                code: 400,
                message: "Name is required",
                error: err
            });

        }

        res.status(200).send({
            code: 200,
            message: "recode inserted"
        })
    });
});

app.post('/create/article', function (req, res) {
    var body = {};
    body.author_id = req.body.author_id;
    body.title = req.body.title;
    body.url = req.body.url;
    body.content = req.body.content;
    // body.createdAt = req.body.createdAt;
    // body.updatedAt = req.body.updatedAt;

    var sql = "INSERT INTO article (author_id, title, url, content, updatedAt) VALUES ('" + body.author_id + "', '" + body.title + "', '" + body.url + "', '" + body.content + "', ' CURRENT_TIMESTAMP()')";
    connect.query(sql, function (err, result) {
        if (!body.author_id) {
            res.status(400).send({
                code: 400,
                message: "author id is required",
                error: err
            });

        }
        res.status(200).send({
            code: 200,
            message: "recode inserted"
        })
    });
});

// read articles

app.get('/read/articles', function (req, res) {

    var sql = "SELECT * FROM article";
    connect.query(sql, function (err, results) {
        if (err) {
            res.status(400).send({
                code: 400,
                message: "error",
                error: err
            })
        }
        res.status(200).send({
            code: 200,
            body: results,
            message: "all articles"
        })
    });
});

// read article

app.get('/read/articles/:id', function (req, res) {

    var sql = "SELECT * FROM article WHERE id = (" + req.params.id + ")";
    connect.query(sql, function (err, results) {
        if (err) {
            res.status(400).send({
                code: 400,
                message: "error",
                error: err
            })
        }
        res.status(200).send({
            code: 200,
            body: results,
            message: "article"
        })
    });
});


// update article

app.patch('/update/article/:id', function (req, res) {
    // var datetime = new Date();
    var body = {};
    body.title = req.body.title;
    body.url = req.body.url;
    body.content = req.body.content;
    body.author_id = req.body.author_id;

    var sql = "UPDATE article SET title = '" + body.title + "', url = '" + body.url + "', content = '" + body.content + "', author_id = '" + body.author_id + "' WHERE id = '" + req.params.id + "'";
    connect.query(sql, function (err, results) {
        if (!body.author_id) {
            res.status(400).send({
                code: 400,
                message: "author id is required",
                error: err
            });
        }
        res.status(200).send({
            code: 200,
            message: "record update"
        })
    });
});

// delete article
app.post('/delete/article/:id', function (req, res) {

    var sql = "DELETE FROM article WHERE id = '" + req.params.id + "'";
    connect.query(sql, function (err, results) {
        if (err) {
            res.status(400).send({
                code: 400,
                message: "error",
                error: err
            })
        }
        res.status(200).send({
            code: 200,
            message: "record delete"
        })
    });
});


// server port
app.listen(3000, function () {
    console.log('started on port 3000');
});
